using System;

namespace Monpoke
{
    public class CommandRunner
    {
        private IGame _game;
        
        public CommandRunner(IGame game)
        {
            _game = game;
        }
        
        public string ProcessCommand(string command)
        {
            var split = command.Split(' ');
            switch (split[0].ToUpper())
            {
                case "CREATE":
                    return _game.Create(split[1], split[2], int.Parse(split[3]), int.Parse(split[4]));
                
                case "ATTACK":
                    return _game.Attack();
                
                case "ICHOOSEYOU":
                    return _game.IChooseYou(split[1]);

                default:
                    throw new Exception("Unknown command :(");
            }
        }
    }
}
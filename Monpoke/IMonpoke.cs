﻿namespace Monpoke
{
    public interface IMonpoke
    {
        string Name { get; }
        int Hp { get; }
        int Ap { get; }
        void ReceiveAttack(int attackPower);
        bool IsDefeated();
    }
}

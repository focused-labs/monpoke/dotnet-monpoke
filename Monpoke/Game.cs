using System;
using System.Collections.Generic;
using System.Linq;

namespace Monpoke
{
    public class Game : IGame
    {
        private const int MAX_TEAMS = 2;
        private List<ITeam> _teams = new List<ITeam>();
        private int currentTeamIndex = 0;
        private bool gameStarted = false;
        private readonly Func<string, ITeam> _teamFactory;

        public Game(Func<string, ITeam> teamFactory)
        {
            _teamFactory = teamFactory;
        }

        public string Create(string teamID, string monpokeName, int hp, int ap)
        {
            if (gameStarted)
            {
                throw new Exception("Can't Create after game started!");
            }
            ITeam team = GetTeam(teamID);
            if (team == null)
            {
                
                if (_teams.Count == MAX_TEAMS)
                    throw new Exception("Too many teams!");
                team = _teamFactory(teamID);
                _teams.Add(team);
            }
            
            team.AddMonpoke(monpokeName, hp, ap);

            return $"{monpokeName} has been assigned to team {teamID}!";
        }

        public string Attack()
        {
            if (!gameStarted)
            {
                throw new Exception("Game hasn't started yet, choose a monpoke first!");
            }
            var attacker = GetCurrentTeam().GetChosenMonpoke();
            if (attacker == null)
            {
                throw new Exception("Can't attack until attacker has chosen a monpoke!");
            }
            if (attacker.IsDefeated())
            {
                throw new Exception("Can't attack with a defeated monpoke!");
            }

            var defender = GetOpposingTeam().GetChosenMonpoke();

            defender.ReceiveAttack(attacker.Ap);
            
            var log = $"{attacker.Name} attacked {defender.Name} for {attacker.Ap} damage!";
            if (defender.IsDefeated())
            {
                log += $"{Environment.NewLine}{defender.Name} has been defeated!";
            }

            if (GetOpposingTeam().IsDefeated())
            {
                log += $"{Environment.NewLine}{GetCurrentTeam().Name} is the winner!";
            }

            NextTurn();

            return log;

        }

        private ITeam GetOpposingTeam()
        {
            return _teams[(currentTeamIndex + 1) % MAX_TEAMS];
        }

        public string IChooseYou(string monpokeName)
        {
            gameStarted = true;
            if (_teams.Count < MAX_TEAMS)
                throw new Exception("Must have 2 teams before starting game");
            GetCurrentTeam().Choose(monpokeName);

            NextTurn();

            return $"{monpokeName} has entered the battle!";
        }

        private void NextTurn()
        {
            currentTeamIndex = (currentTeamIndex + 1) % MAX_TEAMS;
        }

        private ITeam GetTeam(string teamId)
        {
            return _teams.FirstOrDefault(t => t.Name == teamId);
        }

        private ITeam GetCurrentTeam()
        {
            return _teams[currentTeamIndex];
        }

        private ITeam WinningTeam()
        {
            if (GetCurrentTeam().IsDefeated())
            {
                return GetOpposingTeam();
            }
            if (GetOpposingTeam().IsDefeated())
            {
                return GetCurrentTeam();
            }
            return null;
        }

        public bool IsCompleted()
        {
            return gameStarted && WinningTeam() != null;
        }
    }
}
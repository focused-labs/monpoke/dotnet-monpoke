﻿using System.Collections.Generic;

namespace Monpoke
{
    public interface ITeam
    {
        public string Name { get; }
        void AddMonpoke(string name, int hp, int ap);
        IMonpoke GetMonpoke(string name);
        IMonpoke GetChosenMonpoke();
        void Choose(string monpokeName);
        bool IsDefeated();
    }
}

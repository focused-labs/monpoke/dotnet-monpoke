using System;

namespace Monpoke
{
    public class Monpoke : IMonpoke
    {
        private string _name;
        private int _hp;
        private int _ap;
        
        public Monpoke(string name, int hp, int ap)
        {
            if (string.IsNullOrEmpty(name) || hp <= 0 || ap <= 0)
            {
                throw new Exception("Invalid creation data");
            }
            
            _name = name;
            _hp = hp;
            _ap = ap;
        }

        public string Name
        {
            get
            {
                return _name;
            }
        }

        public int Hp
        {
            get
            {
                return _hp;
            }
        }

        public int Ap
        {
            get
            {
                return _ap;
            }
        }

        public void ReceiveAttack(int attackPower)
        {
            _hp -= attackPower;
        }

        public bool IsDefeated()
        {
            return _hp <= 0;
        }
    }
}
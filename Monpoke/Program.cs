﻿using System;

namespace Monpoke
{
    class Program
    {
        static void Main(string[] args)
        {
            var game = new Game(teamId =>
                new Team(teamId, args =>
                    new Monpoke(args.Item1, args.Item2, args.Item3)));
            var commandRunner = new CommandRunner(game);
            string line;
            do {
                line = Console.ReadLine();
                if (line != null)
                    Console.WriteLine(commandRunner.ProcessCommand(line));

                if (game.IsCompleted())
                {
                    Environment.Exit(1);
                }
            } while (line != null);
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;

namespace Monpoke
{
    public class Team : ITeam
    {
        private readonly string _name;
        private Func<(string, int, int), IMonpoke> _monpokeFactory;
        private readonly List<IMonpoke> _monpoke = new List<IMonpoke>();

        private int _selectedIndex = -1;
        
        public Team(string name,
                    Func<(string, int, int), IMonpoke> monpokeFactory)
        {
            _name = name;
            _monpokeFactory = monpokeFactory;
        }

        public void AddMonpoke(string name, int hp, int ap)
        {
            _monpoke.Add(_monpokeFactory((name, hp, ap)));
        }

        public string Name
        {
            get
            {
                return _name;
            }
        }

        public IMonpoke GetMonpoke(string name)
        {
            return _monpoke.FirstOrDefault(it => it.Name == name);
        }

        public IMonpoke GetChosenMonpoke()
        {
            if (_selectedIndex < 0)
                throw new Exception("No monpoke chosen!");
            return _monpoke[_selectedIndex];
        }

        public void Choose(string monpokeName)
        {
            _selectedIndex = _monpoke.FindIndex(m => m.Name == monpokeName);
            if (_selectedIndex < 0)
            {
                throw new Exception("Monpoke not found!");
            }
        }

        public bool IsDefeated()
        {
            foreach (var monpoke in _monpoke)
            {
                if (!monpoke.IsDefeated())
                    return false;
            }

            return true;
        }
    }
}
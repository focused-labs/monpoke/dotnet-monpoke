namespace Monpoke
{
    public interface IGame
    {
        string Create(string teamID, string monpokeName, int hp, int ap);
        string Attack();
        string IChooseYou(string monpokeName);
        bool IsCompleted();
    }
}
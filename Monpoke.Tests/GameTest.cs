using System;
using System.Collections.Generic;
using Moq;
using NUnit.Framework;

namespace Monpoke.Tests
{
    public class GameTest
    {
        private const string ROCKET = "Rocket";
        private const string SPROCKET = "sprocket";
        private const string RYANSAUR = "ryansaur";
        private const int RYANSAUR_AP = 123123;
        private const string GLENSAUR = "glensaur";
        private const string BLANKSAUR = "blanksaur";
        private const string INDEX_ZERO_SAUR = "indexZeroSaur";

        private IGame _game;

        private Mock<IMonpoke> _ryansaurMock;
        private IMonpoke _ryansaur;
        private Mock<ITeam> _teamRocketMock;
        private ITeam _teamRocket;

        private Mock<IMonpoke> _glensaurMock;
        private IMonpoke _glensaur;
        private Mock<ITeam> _teamSprocketMock;
        private ITeam _teamSprocket;

        private IDictionary<string, ITeam> _teams;
        private IList<string> _createdTeamIds;

        private bool _glensaurChosen;
        private bool _ryansaurAttacked;
        private bool _ryansaurAttackDefeatsGlensaur;

        [SetUp]
        public void SetUp()
        {
            _createdTeamIds = new List<string>();

            _ryansaurMock = new Mock<IMonpoke>();
            _ryansaurMock.SetupGet(m => m.Name).Returns(RYANSAUR);
            _ryansaurMock.SetupGet(r => r.Ap).Returns(RYANSAUR_AP);
            _ryansaur = _ryansaurMock.Object;

            _teamRocketMock = new Mock<ITeam>();
            _teamRocketMock.Setup(t =>
                t.GetMonpoke(INDEX_ZERO_SAUR)).Returns(_ryansaur);
            _teamRocketMock.Setup(t =>
                t.GetChosenMonpoke()).Returns(_ryansaur);
            _teamRocketMock.SetupGet(t => t.Name).Returns(ROCKET);
            _teamRocket = _teamRocketMock.Object;

            _glensaurMock = new Mock<IMonpoke>();
            _glensaurMock.SetupGet(m => m.Name).Returns(GLENSAUR);
            _glensaurMock.Setup(m => m.ReceiveAttack(RYANSAUR_AP)).Callback(() =>
            {
                _ryansaurAttacked = true;
            });
            _glensaurMock.Setup(m => m.IsDefeated()).Returns(() =>
                _ryansaurAttacked && _ryansaurAttackDefeatsGlensaur);
            _glensaur = _glensaurMock.Object;

            _teamSprocketMock = new Mock<ITeam>();
            _teamSprocketMock.Setup(t =>
                t.GetMonpoke(INDEX_ZERO_SAUR)).Returns(_glensaur);
            _teamSprocketMock.Setup(t => t.Choose(GLENSAUR)).Callback(() =>
            {
                _glensaurChosen = true;
            });
            _teamSprocketMock.Setup(t =>
                t.GetChosenMonpoke()).Returns(() =>
                    _glensaurChosen ? _glensaur : null);
            _teamSprocketMock.SetupGet(t => t.Name).Returns(SPROCKET);
            _teamSprocketMock.Setup(t => t.IsDefeated()).Returns(() =>
                _ryansaurAttacked && _ryansaurAttackDefeatsGlensaur);
            _teamSprocket = _teamSprocketMock.Object;

            _teams = new Dictionary<string, ITeam>
            {
                { ROCKET, _teamRocket },
                { SPROCKET, _teamSprocket }
            };

            _glensaurChosen = false;
            _ryansaurAttacked = false;
            _ryansaurAttackDefeatsGlensaur = false;

            _game = new Game(id =>
            {
                _createdTeamIds.Add(id);
                return _teams[id];
            });
        }

        [Test]
        public void TestCreatingMultipleMonpokeForSameTeam()
        {
            _game.Create(ROCKET, RYANSAUR, 100, 5);
            _game.Create(ROCKET, GLENSAUR, 100, 5);

            _teamRocketMock.Verify(t =>
                t.AddMonpoke(RYANSAUR, 100, 5), Times.Once);
            _teamRocketMock.Verify(t =>
                t.AddMonpoke(GLENSAUR, 100, 5), Times.Once);
        }

        [Test]
        public void TestMaximumTeamCount()
        {
            _game.Create(ROCKET, RYANSAUR, 100, 5);
            _game.Create(SPROCKET, GLENSAUR, 100, 5);
            
            Assert.Throws<Exception>(() => _game.Create("UhOh", "ShouldNotExist", 100000, 100000));
        }

        [Test]
        public void TestIChooseYou()
        {
            _game.Create(ROCKET, BLANKSAUR, 100, 5);
            _game.Create(ROCKET, RYANSAUR, 100, 5);
            _game.Create(SPROCKET, INDEX_ZERO_SAUR, 101, 10);
            _game.Create(SPROCKET, GLENSAUR, 101, 10);

            _game.IChooseYou(RYANSAUR);

            _teamRocketMock.Verify(t => t.Choose(RYANSAUR), Times.Once);

            _game.IChooseYou(GLENSAUR);

            _teamSprocketMock.Verify(t => t.Choose(GLENSAUR), Times.Once);
        }

        [Test]
        public void TestCreateOnlyAtBeginningOfGame()
        {
            Assert.Throws<Exception>(() => _game.IChooseYou("NoTeamsYet"));
        }
        
        [Test]
        public void TestCantCreateAfterGameStarted()
        {
            _game.Create(ROCKET, "Blanksaur", 100, 5);
            _game.Create(ROCKET, RYANSAUR, 100, 5);

            _game.Create(SPROCKET, "IndexZeroSaur", 101, 10);
            _game.IChooseYou(RYANSAUR);

            Assert.Throws<Exception>(() => _game.Create(SPROCKET, GLENSAUR, 101, 10));
        }

        [Test]
        public void TestAttack()
        {
            _game.Create(ROCKET, RYANSAUR, 100, 5);
            _game.Create(SPROCKET, GLENSAUR, 100, 5);
            _game.IChooseYou(RYANSAUR);
            _game.IChooseYou(GLENSAUR);

            var output = _game.Attack();

            _glensaurMock.Verify(m => m.ReceiveAttack(It.IsAny<int>()), Times.Once);
            Assert.AreEqual(
                $"{RYANSAUR} attacked {GLENSAUR} for {RYANSAUR_AP} damage!",
                output);
        }

        [Test]
        public void TestAttackOnlyAfterChoosing()
        {
            _game.Create(ROCKET, "Blanksaur", 100, 5);
            _game.Create(ROCKET, RYANSAUR, 100, 5);
            _game.Create(SPROCKET, "IndexZeroSaur", 101, 10);
            _game.Create(SPROCKET, GLENSAUR, 101, 10);
            _game.IChooseYou(RYANSAUR);
            
            Assert.Throws<Exception>( () => _game.Attack());
        }
        
        [Test]
        public void TestAttackBeforeStartingGame()
        {
            _game.Create(ROCKET, "Blanksaur", 100, 5);
            _game.Create(ROCKET, RYANSAUR, 100, 5);
            _game.Create(SPROCKET, "IndexZeroSaur", 101, 10);
            _game.Create(SPROCKET, GLENSAUR, 101, 10);
            
            Assert.Throws<Exception>( () => _game.Attack());
        }

        [Test]
        public void GameEndsWhenTeamIsDefeated()
        {
            _ryansaurAttackDefeatsGlensaur = true;

            _game.Create(ROCKET, RYANSAUR, 10, 15);
            _game.Create(SPROCKET, GLENSAUR, 10, 10);
            
            _game.IChooseYou(RYANSAUR);
            
            _game.IChooseYou(GLENSAUR);
            
            Assert.IsFalse(_game.IsCompleted());
             
            _game.Attack();

            Assert.IsTrue(_teamSprocket.IsDefeated());
            Assert.IsTrue(_game.IsCompleted());
        }

        [Test]
        public void TeamMustChooseAgainIfPreviousChosenWasDefeated()
        {
            _ryansaurAttackDefeatsGlensaur = true;

            _game.Create(ROCKET, RYANSAUR, 10, 15);
            
            _game.Create(SPROCKET, GLENSAUR, 10, 10);
            _game.Create(SPROCKET, "Blanksaur", 10, 5);
            
            _game.IChooseYou(RYANSAUR);
            
            _game.IChooseYou(GLENSAUR);
            
            _game.Attack(); // Ryansaur defeats Glensaur
            
            Assert.Throws<Exception>(() => _game.Attack()); // Sprocket attacks with defeated Glensaur
        } 
    }
}
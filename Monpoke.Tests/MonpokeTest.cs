using System;
using NUnit.Framework;

namespace Monpoke.Tests
{
    public class MonpokeTest
    {
        private static readonly string RYANSAUR = "ryansaur";
        private static readonly int RYANSAUR_HP = 123123;
        private static readonly int RYANSAUR_AP = 456456;

        private IMonpoke _monpoke;

        [SetUp]
        public void SetUp()
        {
            _monpoke = new Monpoke(RYANSAUR, RYANSAUR_HP, RYANSAUR_AP);
        }

        [Test]
        public void TestMonpokeDefeatedLogic()
        {
            var monpoke = new Monpoke(RYANSAUR, 10, 10);
            Assert.False(monpoke.IsDefeated());
            monpoke.ReceiveAttack(100);
            Assert.True(monpoke.IsDefeated());
        }

        [Test]
        public void TestValidCreate()
        {
            Assert.Throws<Exception>(() => new Monpoke(RYANSAUR, 0, 1));
            Assert.Throws<Exception>(() => new Monpoke(RYANSAUR, 1, 0));
            Assert.DoesNotThrow(() => new Monpoke(RYANSAUR, 1, 1));
        }

        [Test]
        public void TestProperties()
        {
            Assert.AreEqual(RYANSAUR, _monpoke.Name);
            Assert.AreEqual(RYANSAUR_HP, _monpoke.Hp);
            Assert.AreEqual(RYANSAUR_AP, _monpoke.Ap);
        }

        [Test]
        public void TestReceiveAttack()
        {
            var attackPower = 111;

            _monpoke.ReceiveAttack(attackPower);

            Assert.AreEqual(RYANSAUR_HP - attackPower, _monpoke.Hp);
        }

        [Test]
        public void TestIsDefeated()
        {
            Assert.IsFalse(_monpoke.IsDefeated());

            _monpoke.ReceiveAttack(RYANSAUR_HP);

            Assert.IsTrue(_monpoke.IsDefeated());
        }
    }
}
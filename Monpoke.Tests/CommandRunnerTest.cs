using System;
using System.Collections.Generic;
using Moq;
using NUnit.Framework;

namespace Monpoke.Tests
{
    public class CommandRunnerTest
    {
        private Mock<IGame> _mockGame;
        private CommandRunner _commandRunner;
        
        [SetUp]
        public void Setup()
        {
            _mockGame = new Mock<IGame>();
            _mockGame.Setup(game => game.Create(
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<int>(),
                It.IsAny<int>()
            )).Verifiable();
            _mockGame.Setup(game => game.Attack()).Verifiable();
            _mockGame.Setup(game => game.IChooseYou(It.IsAny<string>())).Verifiable();
            _commandRunner = new CommandRunner(_mockGame.Object);
        }

        [Test]
        public void TestCreateCommand()
        {
            _commandRunner.ProcessCommand("CREATE team1 monpoke1 5 2");
            _mockGame.Verify(game => game.Create(
                "team1",
                "monpoke1",
                5,
                2
            ));
        }
        
        [Test]
        public void TestAttackCommand()
        {
            _commandRunner.ProcessCommand("ATTACK");
            _mockGame.Verify(game => game.Attack());
        }
        
        [Test]
        public void TestIChooseYouCommand()
        {
            _commandRunner.ProcessCommand("ICHOOSEYOU monpoke1");
            _mockGame.Verify(game => game.IChooseYou("monpoke1"));
        }

        [Test]
        public void TestBadCommandThrowsException()
        {
            Assert.Throws<Exception>(() => _commandRunner.ProcessCommand("BADCOMMAND"));
        }

        // TODO: This is an integration test, not a unit test; bring it out of this class
        [Test]
        public void TestBattleLogs()
        {
            var realGame = new Game(teamId =>
                new Team(teamId, args =>
                    new Monpoke(args.Item1, args.Item2, args.Item3)));
            var commandRunner = new CommandRunner(realGame);

            var commandInput = new List<string>()
            {
                "CREATE Rocket Meekachu 3 1",
                "CREATE Rocket Rastly 5 6",
                "CREATE Green Smorelax 2 1",
                "ICHOOSEYOU Meekachu",
                "ICHOOSEYOU Smorelax",
                "ATTACK",
                "ATTACK",
                "ICHOOSEYOU Rastly",
                "ATTACK",
                "ATTACK"
            };

            var commandOutput = new List<string>()
            {
                "Meekachu has been assigned to team Rocket!",
                "Rastly has been assigned to team Rocket!",
                "Smorelax has been assigned to team Green!",
                "Meekachu has entered the battle!",
                "Smorelax has entered the battle!",
                "Meekachu attacked Smorelax for 1 damage!",
                "Smorelax attacked Meekachu for 1 damage!",
                "Rastly has entered the battle!",
                "Smorelax attacked Rastly for 1 damage!",
                "Rastly attacked Smorelax for 6 damage!\nSmorelax has been defeated!\nRocket is the winner!"
            };

            for (var i = 0; i < commandInput.Count; i++)
            {
                Assert.AreEqual(commandOutput[i], commandRunner.ProcessCommand(commandInput[i]));
            }
        }
    }
}
using System;
using System.Collections.Generic;
using Moq;
using NUnit.Framework;

namespace Monpoke.Tests
{
    public class TeamTest
    {
        private const string TEAM_ROCKET = "teamRocket";
        private const string MONPOKE_1 = "monpoke1";
        private const int MONPOKE_1_HP = 111;
        private const int MONPOKE_1_AP = 222;
        private const string MONPOKE_2 = "monpoke2";
        private const int MONPOKE_2_HP = 333;
        private const int MONPOKE_2_AP = 444;

        private Mock<IMonpoke> _monpoke1Mock;
        private IMonpoke _monpoke1;
        private Mock<IMonpoke> _monpoke2Mock;
        private IMonpoke _monpoke2;
        private bool _monpoke1Defeated;
        private bool _monpoke2Defeated;

        private List<(string, int, int)> _factoryInputs;
        private Dictionary<string, IMonpoke> _monpoke;

        private ITeam _team;

        [SetUp]
        public void SetUp()
        {
            _monpoke1Mock = new Mock<IMonpoke>();
            _monpoke1Mock.SetupGet(m => m.Name).Returns(MONPOKE_1);
            _monpoke1Mock.SetupGet(m => m.Hp).Returns(MONPOKE_1_HP);
            _monpoke1Mock.SetupGet(m => m.Ap).Returns(MONPOKE_1_AP);
            _monpoke1Mock.Setup(m => m.IsDefeated()).Returns(() =>
                _monpoke1Defeated);
            _monpoke1 = _monpoke1Mock.Object;

            _monpoke2Mock = new Mock<IMonpoke>();
            _monpoke2Mock.SetupGet(m => m.Name).Returns(MONPOKE_2);
            _monpoke2Mock.SetupGet(m => m.Hp).Returns(MONPOKE_2_HP);
            _monpoke2Mock.SetupGet(m => m.Ap).Returns(MONPOKE_2_AP);
            _monpoke2Mock.Setup(m => m.IsDefeated()).Returns(() =>
                _monpoke2Defeated);
            _monpoke2 = _monpoke2Mock.Object;

            _factoryInputs = new List<(string, int, int)>();
            _monpoke = new Dictionary<string, IMonpoke>
            {
                { MONPOKE_1, _monpoke1 },
                { MONPOKE_2, _monpoke2 }
            };

            _team = new Team(TEAM_ROCKET, args =>
            {
                _factoryInputs.Add(args);
                return _monpoke[args.Item1];
            });
        }

        [Test]
        public void TestAddAndGetMonpoke()
        {
            Assert.IsNull(_team.GetMonpoke(MONPOKE_1));

            _team.AddMonpoke(MONPOKE_1, MONPOKE_1_HP, MONPOKE_1_AP);

            Assert.AreEqual(1, _factoryInputs.Count);
            Assert.AreEqual(MONPOKE_1, _factoryInputs[0].Item1);
            Assert.AreEqual(MONPOKE_1_HP, _factoryInputs[0].Item2);
            Assert.AreEqual(MONPOKE_1_AP, _factoryInputs[0].Item3);
            Assert.AreSame(_monpoke1, _team.GetMonpoke(MONPOKE_1));
        }

        [Test]
        public void TestDefeatedLogic()
        {
            Assert.IsTrue(_team.IsDefeated());

            _team.AddMonpoke(MONPOKE_1, MONPOKE_1_HP, MONPOKE_1_AP);

            Assert.IsFalse(_team.IsDefeated());

            _monpoke1Defeated = true;

            Assert.IsTrue(_team.IsDefeated());

            _team.AddMonpoke(MONPOKE_2, MONPOKE_2_HP, MONPOKE_2_AP);

            Assert.IsFalse(_team.IsDefeated());
        }

        [Test]
        public void TestChooseAndGetChosenMonpoke()
        {
            _team.AddMonpoke(MONPOKE_1, MONPOKE_1_HP, MONPOKE_1_AP);

            Assert.Throws<Exception>(() => _team.GetChosenMonpoke());

            _team.Choose(MONPOKE_1);

            Assert.AreSame(_monpoke1, _team.GetChosenMonpoke());
        }
    }
}